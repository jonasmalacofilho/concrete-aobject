import haxe.macro.Expr;
import haxe.macro.Context;

@:publicFields
class Main {

    // TODO do not allow captured variables other than `this`
    // TODO FVar, access
    // TODO implements, errors
    static macro function concrete(annon : Expr, iface : Expr) {
        //trace(annon);
        //trace(iface);
        switch (annon.expr) {
            case EObjectDecl(fields):
                //for (f in fields)
                //    trace(f);
                var name = "SomeName";
                var cls = macro class Unnamed {
                    public function new() {}
                };
                cls.name = name;
                cls.fields;
                for (f in fields)
                    cls.fields.push({
                        name : f.field,
                        pos : f.expr.pos,
                        kind : switch (f.expr.expr) {
                            case EFunction(_, fexpr):
                                FFun(fexpr);
                            case _:
                                throw "Bad expression " + f.expr;
                        },
                        access : [APublic]
                    });
                Context.defineType(cls);
                var tpath = {pack : [], name : name};
                return macro new $tpath();
            case _:
        }
        return annon;
    }
    
    static function main() {
        trace(concrete({f : function () return 1}, SomeInterface).f());
    }

}

interface SomeInterface {

    function f() : Int;

}

